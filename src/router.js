import Vue from 'vue';
import Router from 'vue-router';

import Login from '@/components/auth/Login';
//import HelloWorld from '@/components/HelloWorld';

Vue.use(Router);

let router =  new Router({
    mode: "history",
    routes: [
        {
            path: "/",
            name: "login",
            alias:"/login",
            component: Login,
            meta: {
                guest: true
            }
        },

        {
            path: "/dashboard",
            name: "dashboard",
            component: () => import('./components/Index'),
            meta: {
                requiresAuth: true
            }
        }
    ]
})

router.beforeEach((to, from, next) => {
    const publicPages = ['/login', '/dashboard'];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = localStorage.getItem('user');
  
    // trying to access a restricted page + not logged in
    // redirect to login page
    if (authRequired && !loggedIn) {
      next('/login');
    } else {
      next();
    }
  });


export default router