export default class User {
    constructor(username,password,fullname,email,level) {
        this.username = username;
        this.password = password;
        this.fullname = fullname;
        this.email = email;
        this.level = level;
    }
}