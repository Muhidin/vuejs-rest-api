export default function authHeader() {
    let user = localStorage.getItem('user');

    if(user) {
        return 'Bearer ' + user.token ;
    }else {
        return null;
    }
}