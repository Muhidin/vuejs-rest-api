import axios from 'axios';
import authHeader from './auth-header';
const API_URL = 'http://localhost:4000/api/auth/';

class AuthService {
    login(user) {
        let data = JSON.stringify({
            "username": user.username,
            "password": user.password
        })
        let config = {
            method: 'post',
            url: API_URL + 'login',
            headers: { 
                'Content-Type': 'application/json'
            },
            data : data
        }
        return axios(config)
            .then(response => {
                if(response.status == true) {
                    localStorage.setItem('user', response)
                }

                return response;
            });
    }

    logout() {
        localStorage.removeItem('user');
    }

    register(user){
        let data = JSON.stringify({
            "username": user.username,
            "password": user.password,
            "fullname": user.fullname,
            "email": user.email,
            "level": user.level
        })
        let config = {
            method: 'post',
            url: API_URL + 'register',
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': authHeader
            },
            data : data
        }
        return axios(config)
    }
}

export default new AuthService;